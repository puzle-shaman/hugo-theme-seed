# Hugo Theme Seed

## About

**[WIP]**
The project is currently in progress.

A starter project to create a Hugo theme.
Feel free to use, fork, improve ;)

## Use

In your project root place, go to the theme directory

    $ cd themes

And clone the theme

    $ git clone ...

In your project config file, replace the theme option

## Edit

Clone the repo

    $ git clone ...

Install the dependencies

    $ npm install

Build and bundle

    $ npm run build

## Roadmap

- Typescript support for Webpack